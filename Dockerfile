#Version 1.0

FROM ubuntu:22.10

ADD ls.sh /root/ls.sh

RUN chmod a+x /root/ls.sh && /root/ls.sh
RUN apt-get update && apt install man -y && yes | unminimize

CMD ["sleep", "infinity"]
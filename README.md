# GNU/Linux de débutant à confirmé en quelques heures

Bonjour et bienvenue dans ce scénario créé par **Jordan ASSOULINE** pour vous aider à maîtriser la commande `ls` sur Linux.

Ainsi nous verrons dans ce scénario, la commande `ls` avec ces différentes options :

 - `-a`: affiche les fichiers y compris ceux qui sont cachés
 - `-l`: affiche les fichiers en indiquant les droits et autorisation, la taille et la date de modification
 - `-h`: affiche la taille des fichiers de manière simple à lire
 - `-R`: affiche la liste des fichiers de manière récursive
 - `-t`: affiche les fichiers en les triant du plus récent au plus ancien


<br/>

# Introduction

Tout d'abord, je vous invite à vérifier que Docker est bien installé sur votre machine.
Pour cela tapez la commande :
`sudo apt update && sudo apt install docker docker.io -y`

Vous pouvez aussi utiliser le docker playground si vous ne souhaitez pas installer ces éléments sur votre propre machine :
https://labs.play-with-docker.com/

Pour lancer l'environnement de l'exercice, vous allez exécuter la commande :

`docker run -it --rm --name man jassouline/ls:latest bash`

Pour sortir de l'environnement, il suffit de taper la commande `exit` ou de taper sur la combinaison de touches : `CTRL + C`

<br/>

# Exercice 1

Dans ce premier exercice, nous allons essayer de comprendre comment utiliser la commande `ls` de manière simple.

La commande `ls` nous permet de lister les fichiers et les dossiers d'un répertoire, ou d'un ensemble de répertoires. Il existe de nombreuses options pour avoir plus de granularités, mais nous commencerons ici par une utilisation basique pour prendre en main la commande.

1. Utilisez la commande `ls` pour afficher la liste des fichiers et répertoires qui existent dans `/tmp/exercices` :

**Q1: Quels sont les répertoires existants dans "/tmp/exercices" ?**
- [ ] livres
- [ ] books
- [ ] etc
- [ ] animaux
- [ ] GNU
- [ ] linux
<details><summary> Montrer la solution </summary>
<p>
ls /tmp/exercices
</p>
<p>
On retrouve ainsi les répertoires : livres, animaux et GNU.
</p>
</details>

**Q2: De la même manière, combien de fichiers existent dans le répertoire "/tmp/exercices/animaux" ?**

<details><summary> Montrer la solution </summary>
<p>
ls /tmp/exercices/animaux
</p>
<p>
On distingue 4 fichiers différents dans ce répertoire : chat, cheval, chien et singe.
</p>
</details>

<br/>

# Exercice 2

Dans ce deuxième exercice, nous allons essayer de comprendre comment utiliser la commande `ls` avec l'option `-a`.

## Utiliser le man pour comprendre une option

Nous allons maintenant utiliser le man pour voir comment fonctionne l'option `-a` de la commande `ls`.

**Q1: Quelle est l'autre manière d'utiliser l'option -a pour la commande ls ?**
- [ ] --author
- [ ] --escape
- [ ] --all
- [ ] --almost-all

<details><summary> Montrer la solution </summary>
<p>
man ls
</p>
<p>
On observe ainsi que l'option --all peut également être utilisée via le raccourci -a.
</p>
</details>

## Utiliser l'option -a de la commande ls

La commande `ls` nous permet de lister les fichiers et les dossiers d'un répertoire, ou d'un ensemble de répertoires. Grâce à l'option `-a`, nous pouvons afficher les fichiers cachés (c'est à dire ceux commençant par un '.')

1. En utilisant l'option `-a`, identifiez les fichiers cachés qui existent dans le répertoire `/tmp/exercices/livres`.

**Q1: Quel est le nom complet du fichier (incluant le .) caché dans le répertoire "/tmp/exercices/livres" ?**

<details><summary> Montrer la solution </summary>
<p>
ls -a /tmp/exercices/livres
</p>
<p>
.cache.epub
</p>
</details>

**Q2: De la même manière, existe-t-il des fichiers cachés dans le répertoire "/tmp/exercices/animaux" ?**

<details><summary> Montrer la solution </summary>
<p>
ls -a /tmp/exercices/animaux
</p>
<p>
On voit qu'il n'y a pas de fichiers cachés dans ce répertoire.
</p>
</details>

<br/>

# Exercice 3

Dans ce troisième exercice, nous allons essayer de comprendre comment utiliser la commande `ls` davec l'option `-l`.

La commande `ls` nous permet de lister les fichiers et les dossiers d'un répertoire, ou d'un ensemble de répertoires.

Grâce à l'option `-l`, nous pouvons afficher des informations complémentaires, comme les autorisations, la taille, le propriétaire ou la date correspondant à chaque fichier et répertoire.

1. Utilisez l'option `-l`, pour identifier le fichier le plus ancien du répertoire `/tmp/exercices/animaux`. (Vous pouvez également utiliser l'option `-t`)

**Q1: Quel est le nom complet du fichier le plus ancien du répertoire "/tmp/exercices/animaux" ?**

<details><summary> Montrer la solution </summary>
<p>
ls -l /tmp/exercices/animaux
</p>
<p>
On voit que le fichier singe date du 18 Décembre 2015.
</p>
</details>

**Q2: De la même manière, existe-t-il des éléments dans le répertoire "/tmp/exercices/GNU" ?**

<details><summary> Montrer la solution </summary>
<p>
ls -l /tmp/exercices/GNU
</p>
<p>
En effet, il y a un répertoire appelé "Linux"
</p>
</details>

**Q3: Si oui, s'agit-il d'un fichier ? d'un répertoire ? "/tmp/exercices/GNU" ?**
- [ ] Répertoire
- [ ] Fichier
- [ ] Lien symbolique
- [ ] Aucun élément

<details><summary> Montrer la solution </summary>
<p>
ls -l /tmp/exercices/GNU
</p>
<p>
On voit que l'élément linux est un répertoire car la ligne commence par d (pour directory):

drwxr-xr-x 2 root root 4096 Jul 30 08:38 linux
</p>
</details>

<br/>


# Exercice 4

Dans ce quatrième exercice, nous allons essayer de comprendre comment utiliser la commande `ls` davec l'option `-h`.

La commande `ls` nous permet de lister les fichiers et les dossiers d'un répertoire, ou d'un ensemble de répertoires.

Grâce à l'option `-h`, nous pouvons afficher la taille des fichiers de manière simple à lire.

1. Utilisez l'option `-h` combinée avec l'option `-l`, pour identifier le fichier le plus volumineux du répertoire `/tmp/exercices/GNU/linux`.

**Q1: Quel est le fichier le plus volumineux du répertoire "/tmp/exercices/GNU/linux" ?**
- [ ] centos.file
- [ ] debian.file
- [ ] ubuntu.file

<details><summary> Montrer la solution </summary>
<p>
ls -lh /tmp/exercices/GNU/linux
</p>
<p>
On voit que le fichier ubuntu.file a une supérieure à 0, alors que les autres fichiers ont une taille de 0 Ko.
</p>
</details>

**Q2: De la même manière, quelle est la taille du fichier "centos.file" dans le répertoire "/tmp/exercices/GNU/linux" ?**

<details><summary> Montrer la solution </summary>
<p>
ls -lh /tmp/exercices/GNU/linux
</p>
<p>
On voit que le fichier centos.file a une taille de 0Ko.
</p>
</details>

<br/>

# Conclusion

7. Tapez la commande `exit` pour sortir du conteneur, ou bien la combinaison de touches `CTRL + c`

